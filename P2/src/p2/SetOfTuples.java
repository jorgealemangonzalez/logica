package p2;

import java.util.HashSet;
import java.util.Set;


public class SetOfTuples {

    public Tuple tuples[];
    private int lastIndex; 
    public Set<String> setOfVariables;
	
    
    public SetOfTuples(){

        lastIndex = -1;
        tuples= new Tuple[0];
        setOfVariables = new HashSet<String>();

    }


    public void deleteTupleI(int index){
        if(index>= 0 && index <= this.lastIndex){
            for(int i = index ; i < this.lastIndex;i++){

                    this.tuples[i]=this.tuples[i+1];
            }
            this.lastIndex--;
        }

    }




    public Tuple getValueTupleI(int i) throws IndexOutOfBoundException {
        if(i > this.lastIndex){
            throw new IndexOutOfBoundException();
        } /*throw exception!*/
        return tuples[i];
    }

    public int len(){
        return lastIndex;
    }

    public void copyFromSetOfTuples(SetOfTuples toCopy ){

        int newLastIndex=-1;
        for (int i =0 ; i <= Math.min(this.lastIndex,toCopy.lastIndex);i++){

            this.tuples[i]=new Tuple(toCopy.tuples[i]);

            newLastIndex=i;
        }

        this.lastIndex=newLastIndex;
        this.setOfVariables=toCopy.setOfVariables;
    }


    public SetOfTuples(SetOfTuples toCopy){


        this.tuples=new Tuple[toCopy.lastIndex+1];
        for(int i = 0; i<=toCopy.lastIndex;i++){
            this.tuples[i]=new Tuple(toCopy.tuples[i]);
        }
        this.lastIndex=toCopy.lastIndex;
        this.setOfVariables=toCopy.setOfVariables;
    }



    public void addValueTuple(Tuple valueTupleToAdd) {
        SetOfTuples oldInstance= new SetOfTuples(this);

        this.tuples= new Tuple[oldInstance.lastIndex+2];

        this.copyFromSetOfTuples(oldInstance);

        this.tuples[lastIndex+1] = valueTupleToAdd;
        this.lastIndex++;
        this.setOfVariables.addAll(valueTupleToAdd.setOfVariables());
    }



    /**/

    public boolean contains(Tuple key){
        int i=0;
        while(i<= this.lastIndex && !this.tuples[i].equals(key)){
            i++;
        }
        
        if (i <= this.lastIndex) return true;
        else return false;
    }

    public void appendSetOfTuples(SetOfTuples toAppend){
        SetOfTuples oldInstance = new SetOfTuples(this);
        this.tuples = new Tuple[oldInstance.lastIndex+1+toAppend.lastIndex+1];
        this.copyFromSetOfTuples(oldInstance);

        for (int i = this.lastIndex+1; i < oldInstance.lastIndex+1+toAppend.lastIndex+1 ;i++){
            this.tuples[i]=new Tuple(toAppend.tuples[i-oldInstance.lastIndex-1]);
        }
        this.lastIndex=oldInstance.lastIndex+1+toAppend.lastIndex;

        this.setOfVariables.addAll(toAppend.setOfVariables);
    }
    
        //Conjunction of two sets of tuples
    public void intersectSetOfTuples(SetOfTuples toIntersect){
        int count = 0;
        boolean[] thereIsnt = new boolean[lastIndex+1];
        for(int i=0; i<=lastIndex; i++){
            thereIsnt[i] = true;
        }
            //Mirar qué tuplas hay en común
        for(int i=0; i<=toIntersect.len(); i++){
            for(int j=0; j<=lastIndex; j++){
                if(thereIsnt[j]){
                    if(toIntersect.tuples[i].equals(this.tuples[j])){
                        thereIsnt[j] = false;
                        count++;
                        break;
                    }
                }
            }
        }
            //Añadir las tuplas en común en el nuevo set
        SetOfTuples oldInstance = new SetOfTuples(this);
        this.tuples = new Tuple[count];
        this.lastIndex = count-1;
        int c = 0;
        for(int i=0; i<=oldInstance.len(); i++){
            if(!thereIsnt[i]){
                this.tuples[c] = new Tuple(oldInstance.tuples[i]);
                c++;
            }
        }
    }
    
        //Disjunction of two sets of tuples
    public void unifySetOfTuples(SetOfTuples toUnify){
            //Debería sumar todas las tuplas del otro set evitando repeticiones
    }

/**/

    /**
     *
     * @param variableName
     * @param inputUniverse
     */
    public void addVariable(String variableName, int[] inputUniverse){

            if(this.lastIndex == -1){this.addValueTuple(new Tuple());}


            SetOfTuples oldInstance = new SetOfTuples(this);
            this.tuples = new Tuple[(this.lastIndex+1)*inputUniverse.length];
            this.lastIndex=-1;


            for (int i = 0; i <= oldInstance.lastIndex;i++){

                    this.appendSetOfTuples(oldInstance.tuples[i].addVariable(variableName, inputUniverse));

            }



    }

    /**/

    public String prettyPrint(){
		
    	String outputString = "";
    	
    	if(len()==-1){outputString = "The list is empty!\n";}
    	else{
    	if(len()>0){outputString+="There are "+(this.len()+1)+" tuples the list:\n";}
    	else {outputString = "There is one tuple in the list:\n";}
	    
	    for(int i = 0; i <= this.lastIndex ; i++){
	    	outputString += this.tuples[i].subPrettyPrint()+"\n";
	    	
	    	
	    }
    	}
	    
		return outputString;
		
	}
    
    public void systemPrettyPrint(){
    	
    	
    	System.out.println(this.prettyPrint());
    	
    }
	
}
