/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

/**
 *
 * @author miquel
 */
public class Conjunction extends SubFormula{
    private SubFormula subf1, subf2;
    
    public Conjunction(Universe u, SubFormula l, SubFormula r){
        super(u);
        subf1 = l;
        subf2 = r;
    }

    public SetOfTuples getSatisfyingTuples() {
        SetOfTuples set = subf1.getSatisfyingTuples();
        set.intersectSetOfTuples(subf2.getSatisfyingTuples());
        return set;
    }
    
    
}
