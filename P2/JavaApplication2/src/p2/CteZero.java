
package p2;

import java.util.HashSet;
import java.util.Set;

public class CteZero extends Function{
    CteZero(){
        super();
    }

    public int Evaluate(int[] termValues) {
        return 0;
    }

    public Set<String> getSetOfVariables() {
        return new HashSet<String>();
    }

    public int getValue(Tuple assignment) {
        int[] empty = new int[0];
        return Evaluate(empty);
    }
    
    
}
