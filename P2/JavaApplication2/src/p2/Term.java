package p2;



import java.util.*;

    /**
     * Clase abstracta para términos.
     *  -getSetOfVariables devuelve el set de variables en este término
     *  -getValue devuelve su valor dada una tupla de valores a variables
     */
public abstract class Term {
    public abstract Set<String> getSetOfVariables();
    public abstract int getValue(Tuple assignment);
}
