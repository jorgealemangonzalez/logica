package p2;



import java.util.HashSet;
import java.util.Set;


public class SetOfTuples {

    public Tuple tuples[];
    private int lastIndex; 
    public Set<String> setOfVariables;	
    
    public SetOfTuples(){

        lastIndex = -1;
        tuples= new Tuple[0];
        setOfVariables = new HashSet<String>();

    }

    public void deleteTupleI(int index){
        if(index>= 0 && index <= this.lastIndex){
            for(int i = index ; i < this.lastIndex;i++){

                    this.tuples[i]=this.tuples[i+1];
            }
            this.lastIndex--;
        }

    }

    public Tuple getValueTupleI(int i){
        if(i > this.lastIndex){
        } /*throw exception!*/
        return tuples[i];
    }

    public int len(){
        return lastIndex;
    }

    public void copyFromSetOfTuples(SetOfTuples toCopy ){

        int newLastIndex=-1;
        for (int i =0 ; i <= Math.min(this.lastIndex,toCopy.lastIndex);i++){

            this.tuples[i]=new Tuple(toCopy.tuples[i]);

            newLastIndex=i;
        }

        this.lastIndex=newLastIndex;
        this.setOfVariables=toCopy.setOfVariables;
    }

    public SetOfTuples(SetOfTuples toCopy){


        this.tuples=new Tuple[toCopy.lastIndex+1];
        for(int i = 0; i<=toCopy.lastIndex;i++){
            this.tuples[i]=new Tuple(toCopy.tuples[i]);
        }
        this.lastIndex=toCopy.lastIndex;
        this.setOfVariables=toCopy.setOfVariables;
    }

    public void addValueTuple(Tuple valueTupleToAdd) {
        SetOfTuples oldInstance= new SetOfTuples(this);

        this.tuples= new Tuple[oldInstance.lastIndex+2];

        this.copyFromSetOfTuples(oldInstance);

        this.tuples[lastIndex+1] = valueTupleToAdd;
        this.lastIndex++;
        this.setOfVariables.addAll(valueTupleToAdd.setOfVariables());
    }
    
    /**/

    public boolean contains(Tuple key){
        int i=0;
        while(i<= this.lastIndex && !this.tuples[i].equals(key)){
            i++;
        }
        
        if (i <= this.lastIndex) return true;
        else return false;
    }

    public void appendSetOfTuples(SetOfTuples toAppend){
        SetOfTuples oldInstance = new SetOfTuples(this);
        this.tuples = new Tuple[oldInstance.lastIndex+1+toAppend.lastIndex+1];
        this.copyFromSetOfTuples(oldInstance);

        for (int i = this.lastIndex+1; i < oldInstance.lastIndex+1+toAppend.lastIndex+1 ;i++){
            this.tuples[i]=new Tuple(toAppend.tuples[i-oldInstance.lastIndex-1]);
        }
        this.lastIndex=oldInstance.lastIndex+1+toAppend.lastIndex;

        this.setOfVariables.addAll(toAppend.setOfVariables);
    }

    /**/

    /**
     *
     * @param variableName
     * @param inputUniverse
     */
    public void addVariable(String variableName, int[] inputUniverse){

            if(this.lastIndex == -1){this.addValueTuple(new Tuple());}


            SetOfTuples oldInstance = new SetOfTuples(this);
            this.tuples = new Tuple[(this.lastIndex+1)*inputUniverse.length];
            this.lastIndex=-1;


            for (int i = 0; i <= oldInstance.lastIndex;i++){

                    this.appendSetOfTuples(oldInstance.tuples[i].addVariable(variableName, inputUniverse));

            }



    }

    /**/

    public String prettyPrint(){
		
    	String outputString = "";
    	
    	if(len()==-1){outputString = "The list is empty!\n";}
    	else{
            if(len()>0){outputString+="There are "+(this.len()+1)+" tuples the list:\n";}
            else {outputString = "There is one tuple in the list:\n";}
	    
	    for(int i = 0; i <= this.lastIndex ; i++){
	    	outputString += this.tuples[i].subPrettyPrint()+"\n";	
	    }
    	}    
	return outputString;	
    }
    
    public void systemPrettyPrint(){
    	System.out.println(this.prettyPrint());	
    }
    
    /**
     * Actualiza las variables para que este set las tenga todas y el otro también
     * @param other
     * @param inputUniverse
     * @return 
     */
    public SetOfTuples intersectionWith(SetOfTuples other,Universe inputUniverse){        
        //Do that both SetsOfTuples has the same variables
        Set<String> variablesDontHaveThis = new HashSet<>(this.setOfVariables);
        variablesDontHaveThis.removeAll(other.setOfVariables);
        for(String variable : variablesDontHaveThis){
            this.addVariable(variable, inputUniverse.valuesList);
        }
        
        Set<String> variablesDontHaveOther = new HashSet<>(other.setOfVariables);
        variablesDontHaveOther.removeAll(this.setOfVariables);
        for(String variable : variablesDontHaveOther){
            other.addVariable(variable, inputUniverse.valuesList);
        }
        
        //Return A SetOfTuples that only have tuples in both sides
        SetOfTuples result = new SetOfTuples();
        for(int i = 0 ; i <= this.lastIndex ; ++i){
            for(int j = i ; j <= other.lastIndex ; ++j){
                if(tuples[i].equals(other.getValueTupleI(j))){
                    result.addValueTuple(tuples[i]);
                }
            }
        }
        return result;
    }
    
    public SetOfTuples unionWith(SetOfTuples other,Universe inputUniverse){
        //Do that both SetsOfTuples has the same variables
        Set<String> variablesDontHaveThis = new HashSet<>(this.setOfVariables);
        variablesDontHaveThis.removeAll(other.setOfVariables);
        for(String variable : variablesDontHaveThis){
            this.addVariable(variable, inputUniverse.valuesList);
        }
        
        Set<String> variablesDontHaveOther = new HashSet<>(other.setOfVariables);
        variablesDontHaveOther.removeAll(this.setOfVariables);
        for(String variable : variablesDontHaveOther){
            other.addVariable(variable, inputUniverse.valuesList);
        }
        //Return A SetOfTuples that have tuples of both sides
        SetOfTuples result = new SetOfTuples();
        for(int i = 0 ; i <= this.lastIndex ; ++i){
            //if(!result.contains(this.getValueTupleI(i))) //teóricamente sobra
                result.addValueTuple(this.getValueTupleI(i));
        }
        for(int j = 0 ; j <= other.lastIndex ; ++j){
           if(!result.contains(other.getValueTupleI(j)))
                result.addValueTuple(other.getValueTupleI(j));
        }
        
        return result;
    }
    
    public SetOfTuples removeVariable(String var){
        SetOfTuples result = new SetOfTuples(this);
        for(Tuple t : result.tuples){
            t.deleteVariable(var);
        }
        return result;
    }
    
    /*
    public boolean containsTuple(Tuple tuple){

        for(Tuple t : tuples){
            if(t.equals(tuple)){
                return true;
            }
        }
        return false;
    }*/
}

