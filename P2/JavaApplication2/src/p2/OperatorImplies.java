/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

/**
 *
 * @author jorge
 */
public class OperatorImplies extends SubFormula{
    SubFormula t1 = null , t2 = null;
    
    public OperatorImplies(SubFormula t1,SubFormula t2,Universe universe){
        super(universe);
        this.t1 = t1;
        this.t2 = t2;
    }
    
    @Override
    public SetOfTuples getSatisfyingTuples(){
        // !t1 OR t2
        
        //!t1
        SetOfTuples result = new OperatorNot(t1,universe).getSatisfyingTuples();
        
        // OR
        return result.unionWith(t2.getSatisfyingTuples(), universe);
    }
}
