/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

import java.util.Set;

/**
 *
 * @author jorgeAleman
 */
public class Multiplication extends Function{
    Term t1,t2;
    public Multiplication(Term t1 , Term t2){
        this.t1 = t1;
        this.t2 = t2;
    }

    @Override
    public int Evaluate(int[] termValues) {
        return termValues[0]*termValues[1];
    }

    @Override
    public Set<String> getSetOfVariables() {
        Set<String> s = t1.getSetOfVariables();
        s.addAll(t2.getSetOfVariables());
        return s;}

    @Override
    public int getValue(Tuple assignment) {
        int[] termValues = new int[2];
        
        termValues[0] = t1.getValue(assignment);
        termValues[1] = t2.getValue(assignment);
        return this.Evaluate(termValues);
    }
    
}
