/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

import java.util.Set;

public class Modulo extends Function{
    private Term dividend;
    private Term divisor;
            
    public Modulo(Term D, Term d){
        dividend = D;
        divisor = d;
    }
    
    public int Evaluate(int[] termValues) {
        if(termValues[1] == 0) return 0;
        return termValues[0]%termValues[1];
    }

    public Set<String> getSetOfVariables() {
        Set<String> setOfVariables = dividend.getSetOfVariables();
        setOfVariables.addAll(divisor.getSetOfVariables());
        return setOfVariables;
    }

    public int getValue(Tuple assignment) {
        int[] termValues = new int[2];
        termValues[0] = dividend.getValue(assignment);
        termValues[1] = divisor.getValue(assignment);
        return Evaluate(termValues);
    }
}
