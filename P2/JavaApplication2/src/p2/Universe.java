package p2;



import java.util.Arrays;


public class Universe {

	private CircularCell currentCell; 
	private CircularCell lastCellAdded;
	private CircularCell headOfTheUniverse;
	public int length;
	public int valuesList[];
	
	private void initializeVariables(){
		
		this.currentCell = null;
		this.length = 0;
		this.lastCellAdded=null;
		this.headOfTheUniverse=null;
		this.valuesList=null;
	}
	
	public Universe(int[] valueList){  //This is the constructor, it takes an array of integers
		
		this.initializeVariables();
		
		
		for(int i =0 ; i < valueList.length ; i++){
			
			this.addValue(valueList[i]);
			
		}
		
		this.valuesList=valueList;
		
	}
	

	
	public Universe(){
		
		this.initializeVariables();
	}
	
	public int[] getValue() throws EmptyUniverseException { //gets the current value pointed to by this universe
		
		if (this.currentCell != null){
			
			return new int[]{this.currentCell.value , this.currentCell.index};
			
		}
		
		
		else throw new EmptyUniverseException();
		
	}
	
	public int[] getNextValue() throws EmptyUniverseException{//get the next value of the universe
		
		if (this.currentCell !=null){
			
			this.currentCell = this.currentCell.nextCell;
			return new int[]{this.currentCell.value,this.currentCell.index};
		}
		
		else throw new EmptyUniverseException();
		
	}
	
	public int[] popValue() throws EmptyUniverseException{//gets the current value and moves by one step
		
		if (this.currentCell !=null){
			
			int[] returnVal = new int[]{this.currentCell.value,this.currentCell.index};
			this.currentCell = this.currentCell.nextCell;
			return returnVal;
		}
		
		else throw new EmptyUniverseException();
		
	}
	
	public void addValue(int valueToAdd){ //you wont need this!
		
		if(this.lastCellAdded!= null){

			CircularCell newCell = new CircularCell(valueToAdd,this.headOfTheUniverse,this.lastCellAdded.index+1);
			newCell.nextCell = this.headOfTheUniverse;
			this.lastCellAdded.nextCell= newCell;
			this.lastCellAdded= newCell;
		    this.length++;
		}
		
		else {this.currentCell = new CircularCell(valueToAdd,null,0);
		this.headOfTheUniverse = this.currentCell;
		this.currentCell.nextCell= this.currentCell;
		this.lastCellAdded= this.currentCell;
		this.length++;
		}
		
	}
	
	public int getCurrentIndex(){
		
		return this.currentCell.index;
	}
	
	public void goToValue(int key){
		
            int i = 0;
            while(i<= this.length && this.currentCell.value != key){
                i++;
                this.currentCell=this.currentCell.nextCell;
            }

	}
}
