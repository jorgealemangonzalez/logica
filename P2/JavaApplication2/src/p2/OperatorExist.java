
package p2;

import java.util.Set;

public class OperatorExist extends SubFormula{
    SubFormula sub;
    Var var;
    public OperatorExist(SubFormula sub, Var var, Universe us){
        super(us);
        this.sub = sub;
        this.var = var;
    }
    
    public SetOfTuples getSatisfyingTuples() {
        return sub.getSatisfyingTuples().removeVariable(var.varName());
    }
}
