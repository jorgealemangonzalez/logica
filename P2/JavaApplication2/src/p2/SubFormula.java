package p2;



/**
 * Clase abstracta para SubFormulas. Sus subclases pueden ser predicados o conectores.
 *  -getSatisfyingTuples devuelve las tuplas con las que la subformula vale true
 *      para ello, la subformula deberá probar las combinaciones que sean necesarias
 */
public abstract class SubFormula {
        protected Universe universe;
        public SubFormula(Universe u){
            universe = u;
        }
	public abstract SetOfTuples getSatisfyingTuples ();
}
