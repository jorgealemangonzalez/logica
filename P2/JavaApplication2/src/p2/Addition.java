package p2;



import java.util.Set;

    /**
     * Ejemplo de función Addition.
     */
public class Addition extends Function {
	private Term[] par;	//parameters
	
	public Addition(Term par1, Term par2){
            par = new Term[2];
            par[0] = par1;
            par[1] = par2;
	}
	
	public int Evaluate(int[] termValues) {
            return termValues[0] + termValues[1];
	}

	public Set<String> getSetOfVariables() {
            Set<String> s = par[0].getSetOfVariables();
            s.addAll(par[0].getSetOfVariables());
            return s;
	}

	public int getValue(Tuple assignment) {
            int[] termValues = new int[2];
            termValues[0] = par[0].getValue(assignment);
            termValues[1] = par[1].getValue(assignment);
            return Evaluate(termValues);
	}

}
