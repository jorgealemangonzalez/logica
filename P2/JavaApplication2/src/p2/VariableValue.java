package p2;



import java.util.Arrays;


public class VariableValue {

    public String variableName; //the name of the variable
    public int value;           //the value assigned to the variable
    private Universe universe;  //on which universe this variable is defined (you should pass the same universe to all variables)

    public VariableValue(String variableName, int value,int[] universe) {//this is the variable constructor
        this.variableName = variableName;

        this.universe = new Universe(universe);
        this.universe.goToValue(value);
        try {
            this.value = this.universe.getValue()[0];
        } catch (EmptyUniverseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
	
    public VariableValue(String variableName, int[] universe) throws EmptyUniverseException{//creates a variable with a radom value, not sure if you will use it!
		
        this.variableName = variableName;

        this.universe = new Universe(universe);

        try{
        this.value = this.universe.getValue()[0];
        } catch(EmptyUniverseException e)
             {throw new EmptyUniverseException();}
    }


    public VariableValue(VariableValue toCopy) {//copy constructor
            this.variableName=toCopy.variableName;
            this.value=toCopy.value;
            this.universe=new Universe(toCopy.universe.valuesList);
            this.universe.goToValue(this.value);
    }

    public String getVariableName() {
            return variableName;
    }

    public int[] getValue() {//gets the current value and the index in the universe
            return new int[]{this.value,this.universe.getCurrentIndex()};
    }


    public int[] nextValue() throws EmptyUniverseException{//gets the current value and the index in the universe
            try{
                this.value= universe.getNextValue()[0];
            }catch(EmptyUniverseException e){System.out.println("empty universe for the variable");}
            try{
                return this.universe.getValue();
            }catch(EmptyUniverseException e){throw new EmptyUniverseException();}
    }



    public int hashCode() {//you wont need this
          return (this.variableName).hashCode();
      }



    public String prettyPrint(){
        return this.variableName+" = "+this.value+",";
    }

}
