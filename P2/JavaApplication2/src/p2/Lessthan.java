package p2;




import java.util.Set;

public class Lessthan extends SubFormula {
	private Term[] par;
	
	Lessthan(Term par1, Term par2, Universe universe){
                super(universe);
		par = new Term[2];
		par[0] = par1;
		par[1] = par2;
	}

            //Debería devolver las tuplas con los valores que cumplen que
            //par[0] < par[1]
        @Override
	public SetOfTuples getSatisfyingTuples(){
            SetOfTuples allComb = new SetOfTuples();
            SetOfTuples result = new SetOfTuples();
            Set<String> vars = par[0].getSetOfVariables();
            vars.addAll(par[1].getSetOfVariables());
            
            for(String var : vars){
                allComb.addVariable(var,universe.valuesList);
            }
            //System.out.print(allComb.prettyPrint());     // DEBUG
            for(int i = 0 ; i <= allComb.len();++i){
                Tuple tup = null;
                tup = allComb.getValueTupleI(i);
                //System.out.println(tup.prettyPrint());    //DEBUG
                if(isLessThan(par[0].getValue(tup), par[1].getValue(tup))){
                    //System.out.println(par[0].getValue(tup)+" > "+ par[1].getValue(tup));
                    result.addValueTuple(tup);
                }
            }
            return result;
	}
        
        private boolean isLessThan(int a , int b){
            return a < b;
        }
}
