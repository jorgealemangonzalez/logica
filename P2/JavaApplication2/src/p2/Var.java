package p2;




import java.util.HashSet;
import java.util.Set;

    /**
     * Para cualquier variable se usa esta clase.
     *  -En var va su nombre
     *      ejemplo: variable "x"
     *  -getSetOfVariables devuelve el set de variables en este término
     *      (en este caso solo devuelve un set con var)
     *  -getValue devuelve su valor dada una tupla de valores a variables
     *      ejemplo: Tupla "x 1, y 2" devuelve 1
     **/

public class Var extends Term {
	private String var;
	
	public Var(String s){
		var = s;
	}
	
        public String varName(){
            return var;
        }
        
	public Set<String> getSetOfVariables() {
		Set<String> s = new HashSet<>();
		s.add(var);
		return s;
	}

	public int getValue(Tuple assignment) {
		return assignment.valueOfVariable(var);
	}
}
