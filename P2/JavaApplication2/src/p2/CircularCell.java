package p2;




public class CircularCell {

	public int value;
	public CircularCell nextCell;
	public int index;
	
	public CircularCell(int value, CircularCell nextCell, int index){
		
		this.value=value;
		this.nextCell = nextCell;
		this.index=index;
		
	}
	
	
}
