package p2;



import java.util.HashSet;
import java.util.Set;


public class Tuple {


    private VariableValue variableValue[]; //the variable values in the tuples
    private int lastIndex; //the size of the tuple-1, the loops go until <= lastIndex
	
    
    public Tuple(){//Constructor
        this.lastIndex = -1;
        this.variableValue= new VariableValue[0];
    }
    
    public Tuple(Tuple toCopy){//copy constructor
	
        this.lastIndex = toCopy.lastIndex;
        this.variableValue = new VariableValue[toCopy.lastIndex+1];
        /**/
        for(int i =0;i<=toCopy.lastIndex;i++){
                this.variableValue[i]= new VariableValue(toCopy.variableValue[i]) ;
        }/**/
    }

    public void copyFromTuple(Tuple toCopy){//copies the tuples passed as argument

        int newLastIndex = -1;

        for(int i =0 ; i <= Math.min(toCopy.lastIndex,this.lastIndex);i++){
            this.variableValue[i]=new VariableValue(toCopy.variableValue[i]);
            newLastIndex=i;
        }

        this.lastIndex=newLastIndex;
    }

    public VariableValue getValueI(int i) {//returns the Ith variableValue
            if(i >= this.lastIndex){}; /*throw exception*/
            return this.variableValue[i];
    }

    public boolean equals( Tuple toCompare ){//compares if two tuples are the same

        if(!(toCompare instanceof Tuple)){
            return false;
        }

        else if(!(this.setOfVariables().equals(toCompare.setOfVariables()))){
            //different variables"
            return false;
        }
        else {
            boolean equal = true;
            for (String var: this.setOfVariables())
            {
                if(!(toCompare.valueOfVariable(var)==this.valueOfVariable(var))){
                    equal=false;
                    break;
                }
            }
            return equal;
        }

    }

    public void addVariable(String variableName, int value, int[] universe) {//adds the variable and its value to the tuple
            VariableValue variableToAdd = new VariableValue(variableName,value,universe);
            Tuple oldInstance = new Tuple(this);
            this.variableValue=new VariableValue[oldInstance.lastIndex+2];
            this.copyFromTuple(oldInstance);
            this.variableValue[this.lastIndex+1] = variableToAdd;
            this.lastIndex++;

    }

    public SetOfTuples addVariable(String variableName, int[] inputUniverse){//creates a set of tuples with all the possible values of the variable name

        SetOfTuples allPossibleValues =new SetOfTuples();

        Universe universe = new Universe(inputUniverse);

        for(int i =0; i < inputUniverse.length;i++){

            Tuple tupleBeingAdded = new Tuple(this);
            try {
                tupleBeingAdded.addVariable(variableName, universe.popValue()[0],inputUniverse);
            } catch (EmptyUniverseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            allPossibleValues.addValueTuple(tupleBeingAdded);
        }
        return allPossibleValues;
    }

    public void deleteVariableI(int index){
        if(index>=0 && index <= this.lastIndex){

            for(int i = index ; i< this.lastIndex;i++){
                this.variableValue[i]=this.variableValue[i+1];
            }
            this.lastIndex--;
        }
    }

    public void deleteVariable(String variableName){//removes the variable variableName

            if(this.variableValue.length>0){
            int index = 0;
            while(index<=this.lastIndex && this.variableValue[index].variableName != variableName ){
                    index++;


            }

            if(index<=this.lastIndex){this.deleteVariableI(index);}

            }
    }

    public int lastIndex(){

            return lastIndex;

    }

    public int valueOfVariable(String variable){//returns the value if the variable name passes as argument
    	
    	int i = 0;
    	while(i <= this.lastIndex && this.variableValue[i].variableName != variable){
    		
    		i++;
    		
    		
    	}
    	if(i<= this.lastIndex){
    	return this.variableValue[i].value;
    	} else return 0;
    }
    
    public Set<String> setOfVariables(){//returns the set of all variable that apprear in the tuple
    	
    	Set<String> setOfVariables = new HashSet<String>();
    	
    	for(int i = 0; i <= this.lastIndex; i++){
    	
    	setOfVariables.add(this.variableValue[i].variableName);
    	
    	}
    	
    	return setOfVariables;
    	
    }
    
    /**/
    public String prettyPrint(){
		
	    String outputString="The values of the variables are: ";
	    
	    
	    for(int i = 0; i <= this.lastIndex ; i++){
	    	outputString += this.variableValue[i].prettyPrint()+" ";
	    	
	    }
	
		return outputString;
		
	}
	
public String subPrettyPrint(){
		
	    /*used in another pretty printer, therefore omits the description*/
	
	String outputString="(";
	    
	    
	    for(int i = 0; i <= this.lastIndex ; i++){
	    	outputString += this.variableValue[i].prettyPrint()+" ";
	    	
	    }
	    outputString+=")";
		return outputString;
		
	}

    public void systemPrettyPrint(){//prints a description of the tuple

            System.out.println(this.prettyPrint());

    }
    
    public boolean isEqual(Tuple t){
        if(this.lastIndex != t.lastIndex)return false;
        for(VariableValue varVal : variableValue){
            for(VariableValue varVal2 : t.variableValue){
                if(varVal.getValue()[0] == varVal2.getValue()[0] && varVal.getVariableName().equals(varVal2.getVariableName())){
                    
                }else return false;     //If the havent the same name and value they are dfferent
            }
        }
        return true;
    }
}
