/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package p2;

/**
 *
 * @author jorge
 */
public class OperatorOr extends SubFormula {
    SubFormula t1 = null , t2 = null;
    
    public OperatorOr(SubFormula t1,SubFormula t2,Universe universe){
        super(universe);
        this.t1 = t1;
        this.t2 = t2;
    }
    
    @Override
    public SetOfTuples getSatisfyingTuples(){
        return t1.getSatisfyingTuples().unionWith(t2.getSatisfyingTuples(), universe);
    }
}
