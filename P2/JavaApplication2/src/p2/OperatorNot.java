package p2;

import java.util.Set;

public class OperatorNot extends SubFormula {
    SubFormula sub;
    public OperatorNot(SubFormula sub,Universe us){
        super(us);
        this.sub = sub ;
    }
    
    public SetOfTuples getSatisfyingTuples() {
        //Usar isEqual de las Tuplas para hacer el set complementario
        SetOfTuples set = sub.getSatisfyingTuples();
        Set<String> setOfVariables = set.setOfVariables;
        SetOfTuples notSet = new SetOfTuples();
        for(String var : setOfVariables){
            notSet.addVariable(var, universe.valuesList);
        }
        
        for(int i=0; i<=notSet.len(); i++){
            for(int j=0; j<=set.len(); j++){
                if(notSet.tuples[i].equals(set.tuples[j])){
                    notSet.deleteTupleI(i);
                }
            }
        }
        
        return notSet;
    }
}
