package p2;




import p2.Term;


    /**
     * Para cualquier función o constante se crea una subclase de Function.
     * -Evaluate calcula el valor de la función dados los valores de sus parametros 
     * -getSetOfVariables devuelve el set de variables en este término
     *      (devuelve los nombres de sus parametros)
     *  -getValue devuelve su valor dada una tupla de valores a variables
     */
public abstract class Function extends Term {
  public abstract int Evaluate(int[] termValues);	
}
