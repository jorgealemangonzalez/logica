
package p2;

import java.util.Set;

public class OperatorForAll extends SubFormula{
    SubFormula sub;
    Var var;
    public OperatorForAll(SubFormula sub, Var var, Universe us){
        super(us);
        this.sub = sub;
        this.var = var;
    }
    
    public SetOfTuples getSatisfyingTuples() {
        int[] values = universe.valuesList;
        SetOfTuples set = sub.getSatisfyingTuples();
        Set<String> varNames = set.removeVariable(var.varName()).setOfVariables;
        SetOfTuples[] sets = new SetOfTuples[values.length];
        for(int v=0; v<values.length; v++){
            sets[v] = new SetOfTuples();
            Tuple t = new Tuple();
            t.addVariable(var.varName(), v, values);
            sets[v].addValueTuple(t);
            for(String namevar : varNames){
                sets[v].addVariable(namevar, values);
            }
            sets[v] = sets[v].intersectionWith(set, universe);
        }
        SetOfTuples result = sets[0].removeVariable(var.varName());
        for(int v=1; v<values.length; v++){
            result = result.intersectionWith(sets[v].removeVariable(var.varName()), universe);
        }
        return result.removeVariable(var.varName());
    }
}
