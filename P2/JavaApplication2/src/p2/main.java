package p2;



/**
 * Resumen estructura:
 * --------------------------------------------
 * Term:
 * -------
 * 				abstract Term
 * 
 * abstract Function
 * 
 * fun1 fun2 fun3...		Var
 * +	*	 -
 * --------------------------------------------
 * Formula:
 * -------
 * 				abstract Subformula
 *
 * pred1 pred2 pred3...		conn1 conn2 conn3...
 * >	 <      =		^      v    exist
 * --------------------------------------------
 */

public class main {
    public static void main(String[] args) {
        //main.tests();
        
        int[] universe = new int[]{0,1,2,3};
        Universe uni = new Universe(universe);
        Term varX = new Var("x");
        Term varY = new Var("y");
        
        //More than
        SubFormula morethan = new Lessthan(varY, varX, uni);
        System.out.print(morethan.getSatisfyingTuples().prettyPrint());
        //Equal
        SubFormula equal = new IsEqual(varX, varY, uni);
        System.out.print(equal.getSatisfyingTuples().prettyPrint());
        //Or
        SubFormula or = new OperatorOr(morethan, equal, uni);
        System.out.print(or.getSatisfyingTuples().prettyPrint());
        //ForAll
        SubFormula forAll = new OperatorForAll(or, (Var)varX, uni);
        System.out.print(forAll.getSatisfyingTuples().prettyPrint());
        //Exist
        SubFormula exist = new OperatorExist(or, (Var)varX, uni);
        System.out.print(exist.getSatisfyingTuples().prettyPrint());
        SubFormula not = new OperatorNot(or, uni);
        //System.out.print(not.getSatisfyingTuples().prettyPrint());
        /*
        //universe
        int[] universe = new int[]{0,1,2,3,4,5};
        Universe uni = new Universe(universe);
        
        Term varX = new Var("x");
        Term varY = new Var("y");
        
        Term add = new Addition(varX, varY);
        Term mul = new Multiplication(varX, varY);
        Term mod = new Modulo(varX, add);
        Term mod2 = new Modulo(varY, mul);
        tests();
        SubFormula lt = new Lessthan(mod, mod2, uni);
        SubFormula exist = new OperatorExist(lt, (Var)varX, uni);
        System.out.print("Exist x : x % (x+y) < y % (x*y)\n"+exist.getSatisfyingTuples().prettyPrint());
        */
        }
    
    public static void tests(){
        //universe
        int[] universe = new int[]{0,1,2,3,4,5};
        Universe uni = new Universe(universe);
        
        SetOfTuples s = new SetOfTuples();
        //new tuple
        Tuple t = new Tuple();
                //Añadir variable con valor a la tupla
        t.addVariable("x", 5, universe);
        t.systemPrettyPrint();

                //Añadir variable con valor a la tupla
        t.addVariable("y", 2, universe);
        t.systemPrettyPrint();
        

                //Añadir tupla t al set de tuplas
        s.addValueTuple(t);
        s.systemPrettyPrint();

                //Añadir variable con multiples valores (en este caso todos los del universo)
        s.addVariable("z", universe);
        s.systemPrettyPrint();

                //Uso de la función Addition con parametros: Variable "x", Addition (Variable "y", Variable "z")
        Addition a = new Addition(new Var("x"), new Addition(new Var("y"), new Var("z")));
        for(Tuple tup: s.tuples){
                System.out.println(a.getValue(tup));
        }
        
        //Less than example
        System.out.println("Lessthan");
        Term t1 = new Var("x") , t2 = new Var("y");
        Lessthan lt = new Lessthan(t1,t2,uni);
        SetOfTuples s1 = lt.getSatisfyingTuples();
        System.out.print(s1.prettyPrint());
        
        
        
        //OperatorNot example
        System.out.println("OperatorNot");
        OperatorNot on1 = new OperatorNot(lt,uni);
        SetOfTuples s3 = on1.getSatisfyingTuples();
        System.out.print("NotLessthan: "+s3.prettyPrint());
        
        //OperatorOR example
        System.out.println("OperatorOr");
        OperatorOr or1 = new OperatorOr(on1,lt,uni);
        SetOfTuples s4 = or1.getSatisfyingTuples();
        System.out.print("NotLesthan or lesthan: "+s4.prettyPrint());
        
        //OperatorAND example
        System.out.println("OperatorAnd");
        OperatorAnd and1 = new OperatorAnd(on1,lt,uni);
        SetOfTuples s5 = and1.getSatisfyingTuples();
        System.out.print("NotLesthan and lesthan: "+s5.prettyPrint());
        
        //OperatorImplies example
        System.out.println("OperatorImplies");
        OperatorImplies implies1 = new OperatorImplies(on1,lt,uni);
        SetOfTuples s6 = implies1.getSatisfyingTuples();
        System.out.print("NotLesthan implies lesthan: "+s6.prettyPrint());
        
        //OperatorForAll example
        System.out.println("OperatorForAll");
        OperatorForAll forall = new OperatorForAll(on1,(Var)t1,uni);
        SetOfTuples s7 = forall.getSatisfyingTuples();
        System.out.print("ForAll Not LessThan(x,y): "+s7.prettyPrint());
        
        System.out.println("OperatorForAll2");
        OperatorForAll forall1 = new OperatorForAll(lt,(Var)t1,uni);
        SetOfTuples s8 = forall1.getSatisfyingTuples();
        System.out.print("ForAll LessThan(x,y): "+s8.prettyPrint());
        
        //OperatorExist example
        System.out.println("OperatorExist");
        OperatorExist exist = new OperatorExist(on1,(Var)t1,uni);
        SetOfTuples s9 = exist.getSatisfyingTuples();
        System.out.print("Exist Not LessThan(x,y): "+s9.prettyPrint());
        
        System.out.println("OperatorExist2");
        OperatorExist exist1 = new OperatorExist(lt,(Var)t1,uni);
        SetOfTuples s10 = exist1.getSatisfyingTuples();
        System.out.print("Exist Not LessThan(x,y): "+s10.prettyPrint());
        
        //Multiplication
        System.out.println("Multiplication");
        Multiplication mu = new Multiplication(t1,t2);
        System.out.println(String.format("Multiplication %s * %s = %s",t.getValueI(0).value,t.getValueI(1).value,mu.getValue(t)));
    }
}
